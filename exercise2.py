numbers = []
strings = []
names = ["John", "Eric", "Jessica"]
numbers.extend([1, 2, 3])
strings.extend(["hello", "world"])
second_name = ["Smith", "Ribentrop", "Bakh"]

print(numbers)
print(strings)
print("The second name on the names list is %s" % second_name)
